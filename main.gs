eval(UrlFetchApp.fetch('https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js').getContentText());
function onOpen() {
  const ui = DocumentApp.getUi();
  ui.createMenu('Encryption')
    .addItem('Encrypt', 'encrypt')
    .addSeparator()
    .addItem('Decrypt', 'decrypt')
    .addToUi();

}

function decrypt() {
  const ui = DocumentApp.getUi();
  var pass = ui.prompt("Please Enter Your Password").getResponseText();
  Logger.log(pass);
  if (hashString(pass) == 1.216985755E9){

    const doc = DocumentApp.getActiveDocument();
    body = doc.getBody();
    decrypted = CryptoJS.AES.decrypt(body.getText(), pass);
    body.replaceText('.+', decrypted.toString(CryptoJS.enc.Utf8));

  } else {
    ui.alert('Wrong Password. Please try again.')
  }
}

function encrypt() {
  const ui = DocumentApp.getUi();
  const doc = DocumentApp.getActiveDocument();
  var pass = ui.prompt("Please Enter Your Password").getResponseText();

  if (hashString(pass) == 1.216985755E9) {
  text = doc.getBody();
  encrypted = CryptoJS.AES.encrypt(text.getText(), pass);
  text.replaceText('.+', encrypted) 
  } else {
    ui.alert('Wrong Password');
  }
}

function hashString (pass) {
    var hash = 0;
    if (pass.length == 0) return hash;
    for (i = 0; i < pass.length; i++) {
        char = pass.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
    return Math.abs(hash); 
}

